barrios = [
    'madrid/arganzuela/acacias/',
    'madrid/arganzuela/chopera/',
    'madrid/arganzuela/delicias/',
    'madrid/arganzuela/imperial/',
    'madrid/arganzuela/legazpi/',
    'madrid/arganzuela/palos-de-moguer/',
    'madrid/barajas/aeropuerto/',
    'madrid/barajas/alameda-de-osuna/',
    'madrid/barajas/campo-de-las-naciones-corralejos/',
    'madrid/barajas/casco-historico-de-barajas/',
    'madrid/barajas/timon/',
    'madrid/barrio-de-salamanca/castellana/',
    'madrid/barrio-de-salamanca/fuente-del-berro/',
    'madrid/barrio-de-salamanca/goya/',
    'madrid/barrio-de-salamanca/guindalera/',
    'madrid/barrio-de-salamanca/lista/',
    'madrid/barrio-de-salamanca/recoletos/',
    'madrid/carabanchel/abrantes/',
    'madrid/carabanchel/buena-vista/',
    'madrid/carabanchel/comillas/',
    'madrid/carabanchel/opanel/',
    'madrid/carabanchel/pau-de-carabanchel/',
    'madrid/carabanchel/puerta-bonita/',
    'madrid/carabanchel/san-isidro/',
    'madrid/carabanchel/vista-alegre/',
    'madrid/centro/chueca-justicia/',
    'madrid/centro/huertas-cortes/',
    'madrid/centro/lavapies-embajadores/',
    'madrid/centro/malasana-universidad/',
    'madrid/centro/palacio/',
    'madrid/centro/sol/',
    'madrid/chamartin/bernabeu-hispanoamerica/',
    'madrid/chamartin/castilla/',
    'madrid/chamartin/ciudad-jardin/',
    'madrid/chamartin/el-viso/',
    'madrid/chamartin/nueva-espana/',
    'madrid/chamartin/prosperidad/',
    'madrid/chamberi/almagro/',
    'madrid/chamberi/arapiles/',
    'madrid/chamberi/gaztambide/',
    'madrid/chamberi/nuevos-ministerios-rios-rosas/',
    'madrid/chamberi/trafalgar/',
    'madrid/chamberi/vallehermoso/',
    'madrid/ciudad-lineal/atalaya/',
    'madrid/ciudad-lineal/colina/',
    'madrid/ciudad-lineal/concepcion/',
    'madrid/ciudad-lineal/costillares/',
    'madrid/ciudad-lineal/pueblo-nuevo/',
    'madrid/ciudad-lineal/quintana/',
    'madrid/ciudad-lineal/san-juan-bautista/',
    'madrid/ciudad-lineal/san-pascual/',
    'madrid/ciudad-lineal/ventas/',
    'madrid/fuencarral/arroyo-del-fresno/',
    'madrid/fuencarral/el-pardo/',
    'madrid/fuencarral/fuentelarreina/',
    'madrid/fuencarral/la-paz/',
    'madrid/fuencarral/las-tablas/',
    'madrid/fuencarral/mirasierra/',
    'madrid/fuencarral/montecarmelo/',
    'madrid/fuencarral/penagrande/',
    'madrid/fuencarral/pilar/',
    'madrid/fuencarral/tres-olivos-valverde/',
    'madrid/hortaleza/apostol-santiago/',
    'madrid/hortaleza/canillas/',
    'madrid/hortaleza/conde-orgaz-piovera/',
    'madrid/hortaleza/palomas/',
    'madrid/hortaleza/pinar-del-rey/',
    'madrid/hortaleza/sanchinarro/',
    'madrid/hortaleza/valdebebas-valdefuentes/',
    'madrid/hortaleza/virgen-del-cortijo-manoteras/',
    'madrid/latina/aguilas/',
    'madrid/latina/aluche/',
    'madrid/latina/campamento/',
    'madrid/latina/cuatro-vientos/',
    'madrid/latina/los-carmenes/',
    'madrid/latina/lucero/',
    'madrid/latina/puerta-del-angel/',
    'madrid/moncloa/aravaca/',
    'madrid/moncloa/arguelles/',
    'madrid/moncloa/casa-de-campo/',
    'madrid/moncloa/ciudad-universitaria/',
    'madrid/moncloa/el-plantio/',
    'madrid/moncloa/valdemarin/',
    'madrid/moncloa/valdezarza/',
    'madrid/moratalaz/fontarron/',
    'madrid/moratalaz/horcajo/',
    'madrid/moratalaz/marroquina/',
    'madrid/moratalaz/media-legua/',
    'madrid/moratalaz/pavones/',
    'madrid/moratalaz/vinateros/',
    'madrid/puente-de-vallecas/entrevias/',
    'madrid/puente-de-vallecas/numancia/',
    'madrid/puente-de-vallecas/palomeras-bajas/',
    'madrid/puente-de-vallecas/palomeras-sureste/',
    'madrid/puente-de-vallecas/portazgo/',
    'madrid/puente-de-vallecas/san-diego/',
    'madrid/retiro/adelfas/',
    'madrid/retiro/estrella/',
    'madrid/retiro/ibiza/',
    'madrid/retiro/jeronimos/',
    'madrid/retiro/nino-jesus/',
    'madrid/retiro/pacifico/',
    'madrid/san-blas/amposta/',
    'madrid/san-blas/arcos/',
    'madrid/san-blas/canillejas/',
    'madrid/san-blas/hellin/',
    'madrid/san-blas/rejas/',
    'madrid/san-blas/rosas/',
    'madrid/san-blas/salvador/',
    'madrid/san-blas/simancas/',
    'madrid/tetuan/bellas-vistas/',
    'madrid/tetuan/berruguete/',
    'madrid/tetuan/cuatro-caminos/',
    'madrid/tetuan/cuzco-castillejos/',
    'madrid/tetuan/valdeacederas/',
    'madrid/tetuan/ventilla-almenara/',
    'madrid/usera/12-de-octubre-orcasur/',
    'madrid/usera/almendrales/',
    'madrid/usera/moscardo/',
    'madrid/usera/orcasitas/',
    'madrid/usera/pradolongo/',
    'madrid/usera/san-fermin/',
    'madrid/usera/zofio/',
    'madrid/vicalvaro/ambroz/',
    'madrid/vicalvaro/casco-historico-de-vicalvaro/',
    'madrid/vicalvaro/el-canaveral-los-berrocales/',
    'madrid/vicalvaro/valdebernardo-valderribas/',
    'madrid/villa-de-vallecas/casco-historico-de-vallecas/',
    'madrid/villa-de-vallecas/ensanche-de-vallecas-la-gavia/',
    'madrid/villa-de-vallecas/santa-eugenia/',
    'madrid/villaverde/butarque/',
    'madrid/villaverde/los-angeles/',
    'madrid/villaverde/los-rosales/',
    'madrid/villaverde/san-andres/',
    'madrid/villaverde/san-cristobal/'
];

module.exports = {
    getBarrios: function(){
        let newArray = barrios;
        
        return newArray;
    }    
}