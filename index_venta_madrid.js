const puppeteer = require('puppeteer-extra')
const StealthPlugin = require('puppeteer-extra-plugin-stealth')
const fs = require('fs');
var userAgent = require('user-agents');
puppeteer.use(StealthPlugin());

const barriosTotal = require('./barrios/barriosMadridVenta.js');
const tiposIdealista = 'venta-viviendas';

let barrios = barriosTotal.getBarrios();

let scrape = async () => {

    const RecaptchaPlugin = require('puppeteer-extra-plugin-recaptcha')
    puppeteer.use(
        RecaptchaPlugin({
            provider: { id: '2captcha', token: 'XXXXXXX' },
            visualFeedback: true // colorize reCAPTCHAs (violet = detected, green = solved)
        })
    );

    const browser = await puppeteer.launch({ headless: true, defaultViewport: null });
    let page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    await page.setUserAgent(userAgent.toString());
    
    //Metemos la cabecera en un primer momento
    let itemsIdealista = [{tipo: 'tipo', barrio: 'barrio', vivienda: 'vivienda', precio: 'precio', descripcion: 'descripcion', compania: 'compania', habitaciones: 'habitaciones', metros_cuadrados: 'metros_cuadrados', observaciones: 'observaciones', publicacionReciente: 'publicacionReciente', id_pagina: 'id_pagina', url: 'url', fecha_scraping: 'fecha_scraping', rebaja: 'rebaja'}];
    let objetoJSON = JSON.stringify(itemsIdealista);
    let csv = convertJSON(objetoJSON);
    fs.appendFileSync('./data/venta_md_12_10.txt', csv);
    fs.appendFileSync('./data/venta_md_12_10.csv', csv);
    
    //Iniciamos el proceso de datos
    for(let j = 0; j < barrios.length; j++){

        let pageCount = 1;
        let maxPageCount = 60;

        //Objeto (Mapa) --> Las keys deben ser únicas, por lo que no habrá dos mismas URLs
        let mapa = new Map();
        let arrayMapa = [];

        for(let z = 0; z < maxPageCount; z++){

            await page.goto(`https://www.idealista.com/${tiposIdealista}/${barrios[j]}pagina-${pageCount}.htm`, {waitUntil: 'domcontentloaded', timeout: 0});
    
            await page.solveRecaptchas();

            let paginaConcreta = page._target._targetInfo.url;

            //Ejecutar este do-while puesto que es posible que, de pronto, salga un captcha que puppeteer no detecte
            let evaluarCaptcha;
            do {
                evaluarCaptcha = await page.evaluate(() => {
                    let captcha = document.getElementById('px-captcha');
                    return captcha;
                });

                if(evaluarCaptcha){
                    page.close();
                    page = await browser.newPage();
                    await page.goto('https://yahoo.es');
                    await page.waitFor(90000);                        

                    z = 0;

                    await page.goto(`https://www.idealista.com/${tiposIdealista}/${barrios[j]}pagina-${pageCount}.htm`, {
                        timeout: 0
                    });
                } else {
                    await page.waitFor(3500);
                    await page.goto(`https://www.idealista.com/${tiposIdealista}/${barrios[j]}pagina-${pageCount}.htm`, {
                        timeout: 0
                    });
                }

            } while (evaluarCaptcha);                    

            //Cuando los ítems del barrio hayan llegado a su fin y vuelvan a la primera página (comportamiento por defecto de Idealista), saltamos de barrio
            if(paginaConcreta.indexOf('pagina') == -1 && pageCount != 1){
                console.log(paginaConcreta + " - " + pageCount, " Si pageCount es diferente a 0-1, > No coge elementos");
                break;
            }

            let pageItems = await page.evaluate(() => {
                let conjuntoNodosPagina = [];
                let items = Array.from(document.querySelectorAll('article.item'));

                items.forEach((item,index) => {
                    //Cogemos los valores DOM de las propiedades
                    let vivienda, precio, rebajasPrecio, rebajasPorcentaje, tiempoPublicacion, compania, detallesUno, detallesDos, detallesTres, descripcion, fecha_scraping;
                    
                    vivienda = item.querySelector('div.item-info-container > a');
                    precio = item.querySelector('div.item-info-container div.price-row span.item-price');
                    descripcion = item.querySelector('div.item-info-container div.item-description p');
                    rebajasPrecio = item.querySelector('div.item-info-container span.pricedown span.pricedown_price');
                    rebajasPorcentaje = item.querySelector('div.item-info-container span.pricedown span.icon-pricedown');
                    compania = item.querySelector('div.item-info-container .logo-branding > a');
                    detallesUno = item.querySelector('div.item-info-container span.item-detail:nth-of-type(1)');
                    detallesDos = item.querySelector('div.item-info-container span.item-detail:nth-of-type(2)');
                    detallesTres = item.querySelector('div.item-info-container span.item-detail:nth-of-type(3)');
                    tiempoPublicacion = item.querySelector('div.item-info-container span.item-detail small.txt-highlight-red'); 

                    /////////////////////
                    /////////////////////
                    let descripcionBuena, rebajasPrecioHtml, rebajasPorcentajeHtml, rebajasConjunto, companiaHtml, recienteHtml, habitaciones = null, metrosCuadrados = null, observacionHtml = null;

                    //Comprobamos descripción
                    if(descripcion){descripcionBuena = descripcion.textContent.replace(/;/g, "|");} else {descripcionBuena = null;}

                    //Comprobamos detalles sobre habitaciones y metros cuadrados
                    let regexp = /\d{2,}\s[m]{0,1}[\W]{0,}$/gi; //Puede que ser que haya un '26 mar' o '04 abr', por ejemplo
                    if(detallesUno){
                        if(detallesUno.textContent.match(regexp)) {
                            metrosCuadrados = detallesUno.textContent;
                        } else if (detallesUno.textContent.indexOf('hab') != -1) {
                            habitaciones = detallesUno.textContent;
                        } else if(detallesUno.textContent.indexOf('ascensor') != -1 || detallesUno.textContent.indexOf('planta') != -1 || detallesUno.textContent.indexOf('bajo') != -1) {
                            observacionHtml = detallesUno.textContent;
                        }
                    }
                    
                    if(detallesDos){
                        if(detallesDos.textContent.match(regexp)) {
                            metrosCuadrados = detallesDos.textContent;
                        } else if (detallesDos.textContent.indexOf('hab') != -1) {
                            habitaciones = detallesDos.textContent;
                        } else if (detallesDos.textContent.indexOf('ascensor') != -1 || detallesDos.textContent.indexOf('planta') != -1 || detallesDos.textContent.indexOf('bajo') != -1) {
                            observacionHtml = detallesDos.textContent;
                        }
                    }
                    
                    if(detallesTres){
                        if(detallesTres.textContent.match(regexp)) {
                            metrosCuadrados = detallesTres.textContent;
                        } else if (detallesTres.textContent.indexOf('hab') != -1) {
                            habitaciones = detallesTres.textContent;
                        } else if (detallesTres.textContent.indexOf('ascensor') != -1 || detallesTres.textContent.indexOf('planta') != -1 || detallesTres.textContent.indexOf('bajo') != -1) {
                            observacionHtml = detallesTres.textContent;
                        }
                    }                   

                    //Comprobamos rebajas y unimos
                    if(rebajasPrecio){rebajasPrecioHtml = rebajasPrecio.textContent;} else {rebajasPrecioHtml = null;}
                    if(rebajasPorcentaje){rebajasPorcentajeHtml = rebajasPorcentaje.textContent;} else {rebajasPorcentajeHtml = null;}
                    if(rebajasPrecioHtml != null || rebajasPorcentajeHtml != null){
                        rebajasConjunto = rebajasPrecioHtml + " " + rebajasPorcentajeHtml;
                    } else {
                        rebajasConjunto = null;
                    }

                    //Comprobamos compañía
                    if(compania){companiaHtml = compania.title.replace(/;/g, "|");} else {companiaHtml = null;}

                    //Comprobamos el tiempo de publicación (recientemente publicado)
                    if(tiempoPublicacion){recienteHtml = tiempoPublicacion.textContent;} else {recienteHtml = null;}

                    //Integramos todas las propiedades en un objeto JavaScript
                    conjuntoNodosPagina.push({vivienda: vivienda.textContent, url: vivienda.href, compania: companiaHtml, precio: precio.textContent, rebaja: rebajasConjunto, publicacionReciente: recienteHtml, descripcion: descripcionBuena, habitaciones: habitaciones, metros_cuadrados: metrosCuadrados, observaciones: observacionHtml, fecha_scraping: fecha_scraping});
                });

                return conjuntoNodosPagina;
            });

            //Si no hay ningún ítem en el barrio, rompemos el bucle y pasamos al siguiente barrio
            if(pageItems.length == 0 && pageCount == 1){
                console.log(paginaConcreta, " ------------ ");
                break;
            }

            let nuevoMapa = new Map();
            let id_pagina = pageCount, fecha_scraping = new Date().toLocaleString();                    

            pageItems.forEach((item,index) => {
                nuevoMapa.set(item.url, {tipo: tiposIdealista, barrio: barrios[j], vivienda: item.vivienda, precio: item.precio, descripcion: item.descripcion, compania: item.compania, habitaciones: item.habitaciones, metros_cuadrados: item.metros_cuadrados, observaciones: item.observaciones, publicacionReciente: item.publicacionReciente, id_pagina: id_pagina, url: item.url, fecha_scraping: fecha_scraping, rebaja: item.rebaja});
            });

            mapa = new Map([...mapa].concat([...nuevoMapa]));

            pageCount++;                                   
        }

        //Una vez recorrido un barrio, añadimos al CSV
        for (let dato of mapa.values()) {
            arrayMapa.push(dato);
        }
        let objetoJSON = JSON.stringify(arrayMapa);
        let csv = convertJSON(objetoJSON);

        fs.appendFileSync('./data/venta_md_12_10.txt', csv);
        fs.appendFileSync('./data/venta_md_12_10.csv', csv);
    }    
    
    browser.close();
    return 'Final del proceso';
}

scrape().then((respuesta) => {
    console.log(respuesta);
});

/* StackOverflow */
function convertJSON(objArray) {
    let array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    let str = '';
    for (let i = 0; i < array.length; i++) {
        let line = '';
        for (let index in array[i]) {
            if (line != '') line += ';'

            line += array[i][index];
        }
        str += line + '\r\n';
    }
    return str;
}